Sorry for the inconvenience. I'm moving all to *Open Source Society* repository:
- https://github.com/open-source-society/bioinformatics

**WORK IS UNDER PROGRESS.**
Most of the things are on the "extras" subfolder.
  Please take a look!

*PS.* OPML is discontinued. Contact me (adrian86 -at- gmail -dot- com) if you
want to peep the latest blogs in genomics, bioinformatics and data science!
