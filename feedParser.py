#!/usr/bin/env python

import listparser

result = listparser.parse('rss.opml')

lastTag = []
for i in result.feeds:
  tag = i.tags[0]
  if tag != lastTag:
    print "* %s" % tag
  print "  * [%s](%s)" % (i.title, i.url)
  lastTag = tag

